<?php

use yii\widgets\ListView;
use yii\helpers\Html;

/**  @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-book">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_book-list']); ?>
</div>
