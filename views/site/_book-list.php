<?php

use \yii\helpers\ArrayHelper;

/**  @var \app\entities\Book $model */

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $model->name; ?></h3>
    </div>
    <div class="panel-body">
        <?= implode(',', ArrayHelper::getColumn($model->bookAuthors, 'name')); ?>
    </div>
</div>