<?php

/**
 * @var \app\entities\Author $model
 */

use \yii\helpers\ArrayHelper;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $model->name; ?></h3>
    </div>
    <div class="panel-body">
        <?= implode(',', ArrayHelper::getColumn($model->authorBooks, 'name')); ?>
    </div>
</div>