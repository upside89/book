<?php

use yii\widgets\ListView;
use yii\helpers\Html;

/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Авторы';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-author">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_author-list']); ?>
</div>
