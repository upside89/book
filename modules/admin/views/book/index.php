<?php

use yii\helpers\Html;
use yii\grid\GridView;

use \app\entities\Author;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\search\BookSeacrch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?></p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'slug',
            'name',
            [
                'attribute' => 'author',
                'value' => function ($model) {
                    $items = [];
                    foreach ($model->bookAuthors as $author) {
                        $items[] = $author->name;
                    }
                    return implode(', ', $items);
                },
                'filter' => ArrayHelper::map(Author::find()->all(), 'id', 'name')
            ],
            'updated_at:datetime',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
