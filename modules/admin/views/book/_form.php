<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\entities\Author;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\entities\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bookAuthors')->dropDownList(ArrayHelper::map(Author::find()->all(),'id','name'),['multiple'=>'multiple']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
