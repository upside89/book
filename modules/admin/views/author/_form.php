<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\entities\Book;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\entities\Author */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="author-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authorBooks')->dropDownList(ArrayHelper::map(Book::find()->all(),'id','name'),['multiple'=>'multiple']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
