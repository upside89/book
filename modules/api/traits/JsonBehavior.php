<?php

namespace app\modules\api\traits;

use yii\filters\ContentNegotiator;
use yii\web\Response;

trait JsonBehavior
{
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

}