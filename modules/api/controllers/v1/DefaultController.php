<?php

namespace app\modules\api\controllers\v1;

use app\modules\api\traits\JsonBehavior;
use yii\rest\Controller;


class DefaultController extends Controller
{
    use JsonBehavior;

    public function actionIndex()
    {
        return [
            'version' => '1.0.0'
        ];
    }
}
