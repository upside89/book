<?php

namespace app\modules\api\controllers\v1;


use app\entities\Book;
use app\modules\api\traits\JsonBehavior;
use yii\rest\ActiveController;


class BookController extends ActiveController
{
    use JsonBehavior;

    public $modelClass = Book::class;
}