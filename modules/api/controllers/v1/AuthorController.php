<?php

namespace app\modules\api\controllers\v1;


use app\entities\Author;
use app\modules\api\traits\JsonBehavior;
use yii\rest\ActiveController;


class AuthorController extends ActiveController
{
    use JsonBehavior;

    public $modelClass = Author::class;
}