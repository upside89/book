<?php

namespace app\entities;

use app\queries\AuthorQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%authors}}".
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property int $updated_at
 * @property int $created_at
 *
 * @property Book[] $authorBooks
 * @property int $authorBooksCount
 */
class Author extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%authors}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slug', 'name'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['slug', 'name'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'name' => 'Name',
            'updated_at' => 'Update At',
            'created_at' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorBooks()
    {
        return $this->hasMany(Book::class, ['id' => 'book_id'])->viaTable('{{%author_book}}', ['author_id' => 'id']);
    }

    public function getAuthorBooksCount()
    {
        return $this->hasMany(Book::class, ['id' => 'book_id'])->viaTable('{{%author_book}}', ['author_id' => 'id'])->count();
    }

    /**
     * {@inheritdoc}
     * @return AuthorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AuthorQuery(get_called_class());
    }
}
