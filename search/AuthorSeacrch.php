<?php

namespace app\search;

use app\entities\Book;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\entities\Author;

/**
 * AuthorSeacrch represents the model behind the search form of `app\entities\Author`.
 */
class AuthorSeacrch extends Author
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'updated_at', 'created_at'], 'integer'],
            [['slug', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Author::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //$query->joinWith(['authorBooksCount']);
        //$query->joinWith(['authorBooks']);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'update_at' => $this->updated_at,
            'create_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
