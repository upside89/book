<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\entities\Book;

/**
 * BookSeacrch represents the model behind the search form of `app\entities\Book`.
 */
class BookSeacrch extends Book
{
    public $author;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'updated_at', 'created_at','author'], 'integer'],
            [['slug', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['bookAuthors']);

        // grid filtering conditions
        $query->andFilterWhere([
            'books.id' => $this->id,
            'books.updated_at' => $this->updated_at,
            'books.created_at' => $this->created_at,
            'author_id' => $this->author,
        ]);

        $query->andFilterWhere(['like', 'books.slug', $this->slug])
            ->andFilterWhere(['like', 'books.name', $this->name]);

        return $dataProvider;
    }
}
