<?php

use yii\db\Migration;

/**
 * Handles the creation of table `author_book`.
 */
class m180801_175056_create_author_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('author_book', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'book_id' => $this->integer(),
        ]);

        $this->addForeignKey('authors_fk', '{{%author_book}}', 'author_id', '{{%authors}}', 'id', 'CASCADE');
        $this->addForeignKey('books_fk', '{{%author_book}}', 'book_id', '{{%books}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('author_book');
    }
}
