<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\entities\Book]].
 *
 * @see \app\entities\Book
 */
class BookQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\entities\Book[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\entities\Book|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
