<?php

namespace app\controllers;

use app\entities\Author;
use app\entities\Book;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionAuthor()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Author::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('author', ['dataProvider' => $dataProvider]);
    }


    public function actionBook()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Book::find(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('book', ['dataProvider' => $dataProvider]);
    }

}
